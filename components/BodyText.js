import React from 'react';
import { Text } from 'native-base';

export default (props) => <Text note={props.note} style={[
    { marginTop: 8, marginBottom: 8 },
]}>{props.children}</Text>