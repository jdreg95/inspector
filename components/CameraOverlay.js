import React from 'react';
import { StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { View, Button, Text, Icon } from 'native-base';

const CameraOverlay = (props) => (
  <View style={style.container}>
    <View style={style.bar}>
        <View style={style.capture}>
          <TouchableOpacity style={style.captureButton} onPress={() => { props.takePictureCallback() }} />
        </View>
    </View>

    {props.children}
  </View>
)

const style = StyleSheet.create({
  container: { 
    flex: 1
  },
  bar: {
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center'
  },
  capture: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  captureButton: {
    width: 75,
    height: 75,
    borderWidth: 4,
    borderColor: '#E9E9E9',
    borderRadius: 100
  },
  decision: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  decisionButton: {
    marginLeft: 10,
    marginRight: 10
  }
});

export default CameraOverlay;