import React from 'react';
import { View } from 'native-base';

export default props => {
  const padded = props.padded ? { margin: 8 } : {};
  const flex = props.flex ? { flex: props.flex } : {};
  const row = props.row ? { flexDirection: 'row', alignItems: 'center' } : {};
  const end = props.end ? { alignItems: 'flex-end'} : {};
  
  return <View style={[padded, flex, row, end, props.style]}>{props.children}</View>
}