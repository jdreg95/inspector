import React from 'react';
import { Text } from 'native-base';

export default (props) => <Text note={props.note} style={[
    { marginTop: 8, marginBottom: 4, fontSize: 16, fontWeight: 'bold' },
    props.style
]}>{props.children}</Text>