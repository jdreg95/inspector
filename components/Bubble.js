import React from 'react';
import { View } from 'react-native';
import { Text } from 'native-base';
import Emoji from 'react-native-emoji';

const Bubble = props => <View style={[style.container, props.style]}>
    {props.children}
</View>

const style = {
    container: {
        backgroundColor: '#FFFFFF',
        elevation: 1,
        borderRadius: 100,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 6,
        paddingBottom: 10,
        paddingLeft: 12,
        paddingRight: 12
    },
    emoji: { fontSize: 20, justifyContent: 'center', alignItems: 'center', marginRight: 10 },
    prediction: { fontSize: 16, justifyContent: 'center', alignItems: 'center', fontWeight: 'bold', marginTop: 5 }
}

export default Bubble;
