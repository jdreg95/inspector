import React from 'react';
import { View, Text } from 'native-base';

export default props => <View style={style.container}>
  {props.children}
</View>

const style = {
  container: { height: 80, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFFFFF', elevation: 2, flexDirection: 'row' }
}