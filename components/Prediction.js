import React from 'react';
import { Text } from 'native-base';
import Box from './Box';

export default (props) => {
    return (
        <Box row>
            <Text note style={{ marginRight: 8 }}>{props.isRobot ? 'Computer' : 'Inspecteur'}</Text>
            {props.children}
        </Box>
    );
} 