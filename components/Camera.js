import React from 'react';
import { RNCamera } from 'react-native-camera';
import { TouchableOpacity } from 'react-native';
import { View } from 'native-base';

export default props => <View style={style.container}>
  <RNCamera
    ref={ref => props.getRef(ref)}
    style={style.camera}
    ratio="16:9"
    type={RNCamera.Constants.Type.back}
    flashMode={RNCamera.Constants.FlashMode.off}
    barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
    onBarCodeRead={props.readQR}
  >
    {props.children}
    <View style={style.capture}>
      <TouchableOpacity style={style.captureButton} onPress={props.onTakePicture} />
    </View>
  </RNCamera>
</View>

const style = {
  container: { flex: 1, justifyContent: 'center', alignItems: 'center', position: 'relative'},
  camera: { position: 'absolute', top: 0, left: 0, bottom: 0, right: 0 },
  capture: { position: 'absolute', justifyContent: 'center', alignItems: 'center', left: 0, bottom: 10, right: 0 },
  captureButton: {
    width: 75,
    height: 75,
    borderWidth: 4,
    borderColor: '#E9E9E9',
    borderRadius: 100
  }
}