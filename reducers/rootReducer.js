import { combineReducers } from 'redux'
import assets from './asset';
import auth from './auth';

// Combine reducers to form root state
export default combineReducers({
  assets,
  auth
});