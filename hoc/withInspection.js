import React from 'react';


// Higher order component that allows for reusable inspection logic
export default function withInspection (WrappedComponent, asset) {
    return class Inspection extends React.Component {
        constructor() {
            super();

            this.state = {
                value: null
            }
        }

        setValue(value) { 
            this.setState({ value: value });
        }

        resetValue() {
            this.setState({
                value: null
            });
        }

        render() {
            return <WrappedComponent reviewValue={this.state.value} setValue={this.setValue.bind(this)} resetValue={this.resetValue.bind(this)} {...this.props}/>
        }
    }
}