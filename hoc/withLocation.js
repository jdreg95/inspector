import React from 'react';


// Higher order component to allow for reusable location awareness
export default function withLocation (WrappedComponent, asset) {
  return class Location extends React.Component {
    state = {
      location: null,
      listenID: null,
      isAndroidPermissionGranted: false
    }

    constructor() {
      super();
    }

    // Get initial position and start listening on mount
    async componentDidMount() {
      this.getPosition();
      this.listen();
    }

    // Get the initial position
    getPosition() {
      navigator.geolocation.getCurrentPosition(location => {
        this.setState({
          location: [location.coords.latitude, location.coords.longitude]
        })
      }, error => {
        console.log(error);
      }, {
        enableHighAccuracy: true
      });
    }

    // Stop listening on unmount
    componentWillUnmount() {
      if (this.state.listenID) {
        this.abort();
      }
    }

    // Start listening to position and set it on every tick
    listen() {
      if (this.state.listenID === null) {
        const listenID = navigator.geolocation.watchPosition(location => {
          this.setState({
            location: [location.coords.latitude, location.coords.longitude]
          });
        }, error => {
          console.log(error);
        }, {
          enableHighAccuracy: true
        });

        this.setState({
          listenID
        });
      }
    }

    // Stop listening to position
    abort() {
      navigator.geolocation.clearWatch(this.state.listenID);
    }

    render() {
      return <WrappedComponent currentLocation={this.state.location} {...this.props}/>
    }
  }
}