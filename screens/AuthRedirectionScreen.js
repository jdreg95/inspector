import React from 'react';
import { Container, Spinner } from 'native-base'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { refresh } from '../actions/user';


// Simple redirection component that checks authentication on startup
@connect(({auth}) => ({ refreshToken: auth.user.refreshToken }), dispatch => bindActionCreators({ refresh }, dispatch))
class AuthRedirectionScreen extends React.Component {
  constructor(props) {
    super(props);
    this.bootstrapAuth();
  }

  // If a refresh token is available, try to refresh token and login automatically, else go to login screen
  async bootstrapAuth() {
    if (this.props.refreshToken) {
      try {
        await this.props.refresh();
        this.props.navigation.navigate('Authorized');
      } catch (e) {
        this.props.navigation.navigate('SignIn');
      }
    } else {
      this.props.navigation.navigate('SignIn');
    }
  }

  render() {
    return (
      <Container>
        <Spinner />
      </Container>
    )
  }
}

export default AuthRedirectionScreen;
