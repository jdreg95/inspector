import * as authAPI from "./auth.js";

/*
 * A snippet created by Damon, a Teqplay developer
 * Changed to be usable within the project's context.
 */

export const authFetch = fetchAndRefresh;

// Fetch from a certain URL with certain options and refresh automatically if the access token is not accepted.
async function fetchAndRefresh(uri, options = {}, refreshToken, onChange) {
  try {
    // Initial fetch
    return await fetch(uri, options).then(response => {
      // Fetch succeeded
      if (response.status >= 200 && response.status < 300) {
        return response;
      } else {
        // Fetch failed
        return Promise.resolve(response.text())
          .then(data => {
            const exception = new Error(data)

            exception.status = response.status
            exception.statusCode = data.status

            throw exception
          });
      }
    })
  }
  catch (fetchError) {
    // If the fetch failed due to auth, refresh and try again, else throw
    if (fetchError.status === 401) {
      try {
        const { userID, username, accessToken } = await authAPI.refresh(refreshToken)
        onChange(accessToken);
        const newOptions = { ...options }
        newOptions.headers.Authorization = `Bearer ${accessToken}`;

        return await authFetch(uri, newOptions)
      } catch (refreshError) {
        throw refreshError
      }
    }
    else {
      throw fetchError
    }
  }
}