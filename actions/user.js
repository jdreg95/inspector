import * as authAPI from '../api/auth';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const REFRESH_USER = 'REFRESH_USER_REQUEST';
export const REFRESH_USER_SUCCESS = 'REFRESH_USER_SUCCESS';
export const REFRESH_USER_FAILURE = 'REFRESH_USER_FAILURE';

export const SET_USER_AUTH = 'SET_USER_AUTH';

export const loginUser = active => ({
  type: LOGIN_USER,
  payload: {active}
});

export const loginUserSuccess = user => ({
  type: LOGIN_USER_SUCCESS
});

export const loginUserFailure = error => ({
  type: LOGIN_USER_FAILURE,
  payload: {
    error
  }
});

export const setAuth = payload => ({
  type: SET_USER_AUTH,
  payload
}); 

// Handle state mutation for logging in.
export const login = (username, password) => async dispatch => {
  dispatch(loginUser(true));

  try {
    const data = await authAPI.login(username, password);
    dispatch(loginUserSuccess());
    dispatch(setAuth(data));
  } catch (error) {
    dispatch(loginUserFailure('Error!'))
    throw error;
  } finally {
    dispatch(loginUser(false));
  }
};

export const refreshUser = active => ({
  type: REFRESH_USER,
  payload: {active}
});

export const refreshUserSuccess = user => ({
  type: REFRESH_USER_SUCCESS
});

export const refreshUserFailure = error => ({
  type: REFRESH_USER_FAILURE,
  payload: {
    error
  }
});

// Handle state mutation for refreshing.
export const refresh = () => async (dispatch, getState) => {
  dispatch(refreshUser(true));

  try {
    const data = await authAPI.refresh(getState().auth.user.refreshToken);

    dispatch(refreshUserSuccess())
    dispatch(setAuth(data))
  } catch (error) {
    console.log(error)
    refreshUserFailure(error)
    throw error;
  } finally {
    dispatch(refreshUser(false));
  }
};
