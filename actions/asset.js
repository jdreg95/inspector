import * as assetsAPI from '../api/assets';
import { setAuth } from './user';

export const FETCH_ASSETS_SUCCESS = 'FETCH_ASSETS_SUCCESS';
export const FETCH_ASSETS = 'FETCH_ASSETS';
export const FETCH_ASSETS_FAILURE = 'FETCH_ASSETS_FAILURE';

export const CREATE_ASSET_REVIEW = 'CREATE_ASSET_REVIEW';
export const CREATE_ASSET_REVIEW_FAILURE = 'CREATE_ASSET_REVIEW_FAILURE';
export const CREATE_ASSET_REVIEW_SUCCESS = 'CREATE_ASSET_REVIEW_SUCCESS';

export const CREATE_ASSET = 'CREATE_ASSETS';
export const CREATE_ASSET_SUCCESS = 'CREATE_ASSET_SUCCESS';
export const CREATE_ASSET_FAILURE = 'CREATE_ASSET_FAILURE';

const fetchAssetsCreator = () => ({
  type: FETCH_ASSETS
})
const fetchAssetsSuccess = (assets) => ({
  type: FETCH_ASSETS_SUCCESS,
  payload: { assets }
});

const fetchAssetsFailure = (error) => ({
  type: FETCH_ASSETS_FAILURE,
  payload: { error }
});

// Handle state mutation for fetching assets.
export const fetchAssets = () => async (dispatch, getState) => {
  dispatch(fetchAssetsCreator());

  const { user } = getState().auth;

  try {
    const assets = await assetsAPI.fetchAssets(user.accessToken, user.refreshToken, () => dispatch(setAuth));
    dispatch(fetchAssetsSuccess(assets));
  } catch(error) {
    dispatch(fetchAssetsFailure(error))
    throw error;
  }
}

const createAssetReviewCreator = (id, review) => ({
  type: CREATE_ASSET_REVIEW,
  payload: {
    id,
    review
  }
})

const createAssetReviewSuccess = () => ({
  type: CREATE_ASSET_REVIEW_SUCCESS
});

const createAssetReviewFailure = (error) => ({
  type: CREATE_ASSET_REVIEW_FAILURE,
  payload: { error }
});

// Handle state mutation for creating asset reviews.
export const createAssetReview = (id, review) => async (dispatch, getState) => {
  dispatch(createAssetReviewCreator(id, review));

  const { user } = getState().auth;
  
  try {
    const assets = await assetsAPI.createAssetReview(id, review, user.accessToken, user.refreshToken, () => dispatch(setAuth));
    dispatch(createAssetReviewSuccess());
  } catch(error) {
    dispatch(createAssetReviewFailure(error))
  }
}

const createAssetCreator = (asset) => ({
  type: CREATE_ASSET,
  payload: {
    ...asset
  }
})

const createAssetSuccess = () => ({
  type: CREATE_ASSET_SUCCESS
});

const createAssetFailure = (error) => ({
  type: CREATE_ASSET_FAILURE,
  payload: { error }
});

// Handle state mutation for creating assets.
export const createAsset = (asset) => async (dispatch, getState) => {
  dispatch(createAssetCreator(asset));

  const { user } = getState().auth;
  
  try {
    const actualAsset = await assetsAPI.createAssetFull(asset, user.accessToken, user.refreshToken, () => dispatch(setAuth));
    dispatch(createAssetSuccess());
  } catch(error) {
    dispatch(createAssetFailure(error))
  }
}