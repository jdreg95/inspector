import React from 'react';
import { SwitchNavigator, StackNavigator, TabNavigator } from 'react-navigation';

import AuthRedirectionScreen from './screens/AuthRedirectionScreen';
import SigninScreen from './screens/SigninScreen';
import InspectionScreen from './screens/InspectionScreen';
import CameraScreen from './screens/CameraScreen';
import InspectionModal from './screens/InspectionModal';
import IntroductionScreen from './screens/IntroductionScreen';
import IntroductionRedirectionScreen from './screens/IntroductionRedirectionScreen';
import InformationModal from './screens/InformationModal';
import NENScreen from './screens/NENScreen';

// Setup tab navigation
const MainTabNav = TabNavigator({ 
  PendingInspections: InspectionScreen,
  Camera: CameraScreen
}, {
  tabBarOptions: {
    indicatorStyle: {
      backgroundColor: '#64b5f6',
      height: 5
    },
    labelStyle: {
      color: '#000000',
      fontWeight: 'bold'
    },
    style: {
      backgroundColor: '#FFFFFF'
    }
  }
});

// Setup main app stack
const MainAppStack = StackNavigator({
  Main: MainTabNav,
  NewInspection: InspectionModal,
  Information: InformationModal,
  NENScreen: NENScreen
});

// Setup app entry stack (handles tutorial)
const AppStack = StackNavigator({
  IntroductionRedirection: IntroductionRedirectionScreen,
  Introduction: IntroductionScreen,
  App: MainAppStack,
}, { headerMode: 'none', initialRouteName: 'IntroductionRedirection' });

// Setup root stack (handles auth)
const RootStack = SwitchNavigator({
  AuthorizationRedirection: AuthRedirectionScreen, 
  Authorized: AppStack,
  SignIn: SigninScreen
}, { initialRouteName: 'AuthorizationRedirection' });

const routes = RootStack;

export default routes;
